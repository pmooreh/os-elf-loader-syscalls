#ifndef _VMM_H_
#define _VMM_H_

#include "stdint.h"
#include "refs.h"
#include "fs.h"
#include "locks.h"

class BlockingLock;
class Event;
class File;

// range node, linked list
class SegmentNode {
public:
    StrongPtr<File> file;
    uint32_t va;
    uint32_t fileOffset;
    uint32_t segmentSizeInFile;
    uint32_t segmentSizeInMemory;
    StrongPtr<SegmentNode> next;

    SegmentNode(StrongPtr<File> f, uint32_t v, uint32_t o, uint32_t sf, uint32_t sm);
    virtual ~SegmentNode();
    void printNode();
};

class SegmentList {
    StrongPtr<SegmentNode> head;
    StrongPtr<SegmentNode> last;
    StrongPtr<BlockingLock> lock;
public:
    SegmentList();
    virtual ~SegmentList();
    StrongPtr<SegmentNode> getSegmentNode(uint32_t va);
    void addSegmentNode(StrongPtr<SegmentNode> node);
};

// The physical memory interface
class PhysMem {
public:
    static constexpr uint32_t FRAME_SIZE = (1 << 12);
    static void init(uint32_t start, uint32_t end);

    /* allocate a frame */
    static uint32_t alloc();

    /* free a frame */
    static void free(uint32_t);
};

class AddressSpace {
    uint32_t *pd;
    uint32_t& getPTE(uint32_t va);
    StrongPtr<BlockingLock> lock;
public:
    static constexpr uint32_t P = 1;
    static constexpr uint32_t W = 2;
    static constexpr uint32_t U = 4;

    AddressSpace();
    virtual ~AddressSpace();
    void pmap(uint32_t va, uint32_t pa, bool forUser, bool forWrite);
    void handlePageFault(uint32_t va);
    void activate();
    void fork(StrongPtr<AddressSpace> child);
};

#endif
