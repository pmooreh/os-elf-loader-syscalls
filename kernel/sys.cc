#include "sys.h"
#include "stdint.h"
#include "idt.h"
#include "debug.h"
#include "thread.h"
#include "collection.h"
#include "locks.h"


int SYS::eventId;
BlockingLock* SYS::lock;
StrongPtr<Event>* SYS::eventArr;

struct SwitchParam {
	uint32_t pc;
	uint32_t esp;
	uint32_t eax;
	SwitchParam(uint32_t pc, uint32_t esp, uint32_t eax)
		: pc(pc), esp(esp), eax(eax) {}
	~SwitchParam() {}
};

void forkSwitch(StrongPtr<SwitchParam> param) {
	switchToUser(param->pc, param->esp, param->eax);
}

/*
	my research shows that magic[3] has the stack frame
	we want, and stack[1] has the variable we want! yay

number   signature                        description
0        void exit();                     terminate the current thread
1        void putch(char c)               print 'c' to the console
2        int id= fork()                   returns twice
                                              id == 0 --> child
                                              id != 0 --> parent 
3        int id = semaphore(int count)    creates a semaphore with the
                                          given count
4        void up(int id)                  up
5        void down(int id)                down
6        void shutdown()                  shutdown
7        void join(int id)                block until the given process

*/
extern "C" int sysHandler(uint32_t eax, uint32_t *magic) {
	uint32_t* userStack = (uint32_t*)magic[3];
	uint32_t arg = userStack[1];
    switch (eax) {
    	case 0: {
    		Thread::exit();
    		return 0;
    		break;
    	}
		case 1: {
			Debug::printf("%c", arg);
			return 0;
			break;
		}
		case 2: {

			StrongPtr<SwitchParam> param = StrongPtr<SwitchParam>(new SwitchParam(magic[0], magic[3], 0));
			// create new process
			StrongPtr<Process> child = StrongPtr<Process>(new Process());
			// copy the address space
			Thread::current()->process->addressSpace->fork(child->addressSpace);
			// create new thread
			Thread* cThread = new FuncThread<StrongPtr<SwitchParam>>(child, forkSwitch, param);
			// now add child's thread's exitEvent; will serve as the id of the child
			int cId = getThenIncrement(&SYS::eventId, 1);
			SYS::eventArr[cId] = cThread->exitEvent;
			// make this thread run!!!
			Thread::start(cThread);
			// Debug::printf("    Process gets id: %d\n", id);
			return cId;
			break;
		}
		case 3: {
			StrongPtr<Semaphore> sema = StrongPtr<Semaphore>(new Semaphore(arg));
			int id = Process::semaphores()->add(sema);
			return id;
			break;
		}
		case 4: {
			Process::semaphores()->get(arg)->up();
			return 0;
			break;
		}
		case 5: {
			Process::semaphores()->get(arg)->down();
			return 0;
			break;
		}
		case 6: {
			Debug::panic("shut 'er down");
			return 0;
			break;
		}
		case 7: {
			// Debug::printf("    wait on Process with id: %d\n", arg);
			SYS::eventArr[arg]->wait();
			SYS::eventArr[arg].reset();
			return 0;
			break;
		}
	    default:
	        Debug::panic("unknown system call %d",eax);
	        return 0;
    }
    
}

extern "C" void sysAsmHandler();

void SYS::init(void) {
    IDT::trap(48,(uint32_t)sysAsmHandler,3);

    eventId = 1;
    lock = new BlockingLock();
    eventArr = new StrongPtr<Event>[2020];

}

