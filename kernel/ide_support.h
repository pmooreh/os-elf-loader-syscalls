#ifndef _IDE_SUPPORT_H_
#define _IDE_SUPPORT_H_

#include "stdint.h"

constexpr size_t IDE_SECTOR_SIZE = 512;

extern void readSector(uint32_t drive, uint32_t sector, uint32_t* buffer);
extern void ideStats(void);

#endif
