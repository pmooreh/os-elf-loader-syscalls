#include "collection.h"



template <class T>
int Collection<T>::getLowestAvailable() {
    for (int i = 0; i < size; i++) {
        if (allocated[i] == false)
            return i; // found lowest
    }
    return -1; // failed
}

template <class T>
Collection<T>::Collection(int size)
        : size(size), arr(new StrongPtr<T>[size]), allocated(new bool[size]), lock(StrongPtr<BlockingLock>(new BlockingLock())) {}

template <class T>
StrongPtr<T> Collection<T>::get(int id) {
    lock->lock();

    if (allocated[id - 1] == false) {
        lock->unlock();
        return StrongPtr<T>(); // this id is invalid, give null
    }

    lock->unlock();
    return arr[id - 1];
}

template <class T>
int Collection<T>::add(StrongPtr<T> elem) {
    lock->lock();
    int lowest = getLowestAvailable();

    if (lowest == -1) {
        lock->unlock();
        return -1; // no space right now, failed
    }

    allocated[lowest] = true;
    arr[lowest] = elem;
    lock->unlock();
    return lowest + 1;
}

template <class T>
bool Collection<T>::remove(int id) {
    lock->lock();

    if (allocated[id - 1] == false) {
        lock->unlock();
        return false; // can't remove an element that doesnt exist
    }

    allocated[id - 1] = false;
    arr[id - 1].reset();
    lock->unlock();
    return true;
}

template class Collection<Semaphore>;
template class Collection<Event>;
