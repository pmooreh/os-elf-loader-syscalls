#include "process.h"
#include "machine.h"
#include "collection.h"


/***********************************
         Process
***********************************/

struct ProcessInfo {
    StrongPtr<Process> kernelProcess;
    StrongPtr<Collection<Semaphore>> semaphores;
    // StrongPtr<Collection<Event>> events;
    volatile int nextId;
};

static ProcessInfo *info;

void Process::init(void) {
    info = new ProcessInfo();
    info->semaphores = StrongPtr<Collection<Semaphore>>(new Collection<Semaphore>(100));
    // info->events = StrongPtr<Collection<Event>>(new Collection<Event>(100));
    info->nextId = 0; 
    info->kernelProcess = StrongPtr<Process> { new Process() };
}

StrongPtr<Process> Process::kernel(void) {
    return info->kernelProcess;
}

Process::Process() : addressSpace(new AddressSpace()),
    id(getThenIncrement(&info->nextId,1)) {}

// StrongPtr<Collection<Event>> Process::events() {
// 	return info->events;
// }

StrongPtr<Collection<Semaphore>> Process::semaphores() {
    return info->semaphores;
}

Process::~Process() {
    // signal the exit event
    // Debug::printf("\n");
    // Debug::printf("PROCESS DESTRUCTOR, has id: %d\n", eventId);
    // Debug::printf("\n");
    
    // Process::events()->get(eventId)->signal();
}
