#ifndef _PIT_H_
#define _PIT_H_

#include "stdint.h"

class Pit {
    static uint32_t ticksPerSecond;
public:
    static void init(uint32_t hz);
    static uint32_t ticks;
    static uint32_t secondsToTicks(uint32_t secs);
};

#endif
