#ifndef _FS_H_
#define _FS_H_

#include "device.h"

class FileSystem;
class BlockIO;

/**********/
/* A file */
/**********/

class File {
public:
    File() {}
    virtual ~File() {}

    /* We can ask it for its type */
    virtual uint32_t getType() = 0;    /* 1 => directory, 2 => file */

    /* We can ask it for its length in bytes */
    virtual uint32_t getLength() = 0;

    /* read up to nbyte starting at offset, never crosses block boundaries */
    /* returns the number of bytes actually read */
    virtual size_t read(size_t offset, void* buf, size_t nbyte) = 0;

    /* read nbyte starting at offset */
    /* returns nbyte unless an error occurred */
    virtual size_t readAll(size_t offset, void* buf, size_t nbyte) = 0;
};

/***************/
/* A directory */
/***************/

class Directory {
public:
    Directory() {}
    virtual ~Directory() {}

    /* We can lookup a file */
    virtual StrongPtr<File> lookupFile(const char* name) = 0;

    /* And a directory */
    virtual StrongPtr<Directory> lookupDirectory(const char *name) = 0;

    /* number of entries */
    virtual size_t nEntries() = 0;

    /* get zero-terminated name of entry */
    virtual void entryName(size_t i, char* name) = 0;
};

/***************/
/* File system */
/***************/

class FileSystem {
public:
     StrongPtr<BlockIO> dev;
     StrongPtr<Directory> rootdir;           // the root directory

     FileSystem(StrongPtr<BlockIO> dev) : dev(dev) {}
};

#endif
