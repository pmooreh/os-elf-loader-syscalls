#include "fat439.h"
#include "machine.h"
#include "stdint.h"

/*************************/
/* Fat439 implementation */
/*************************/

static uint32_t min(uint32_t a, uint32_t b) {
    return (a < b) ? a : b;
}

class Fat439File : public File, public BlockIO {
    StrongPtr<BlockIO> dev;
    uint32_t start;
    Fat439* fat439;
    struct {
        uint32_t type;
        uint32_t length;
    } metaData;
public:
    Fat439File(StrongPtr<BlockIO> dev, uint32_t start, Fat439* fat439) :
        dev(dev),start(start),fat439(fat439)
    {
        dev->readAll(start * BlockIO::BLOCK_SIZE,
            &metaData, sizeof(metaData));
    }
    virtual uint32_t getLength() { return  metaData.length; }
    virtual uint32_t getType() { return metaData.type; }

    virtual StrongPtr<BlockBuffer> readBlock(size_t num) override {
        uint32_t blockNumber = start;

        for (uint32_t i=0; i<num; i++) {
            blockNumber = fat439->fat[blockNumber];
            if (blockNumber == 0) {
                Debug::printf("block %d\n",blockNumber);
                return StrongPtr<BlockBuffer>();
            }
        }
        return dev->readBlock(blockNumber);
    }

    virtual size_t read(size_t offset_, void* buf, size_t nbyte_) override {
        size_t end_ = min(offset_ + nbyte_ , getLength());
        size_t offset = offset_ + 8;
        size_t end = end_ + 8;
        if (offset > end) return 0;
        size_t nbyte = end - offset;
        size_t n = BlockIO::read(offset,buf,nbyte);
        return n;
    }

    virtual size_t readAll(size_t offset, void* buf, size_t nbyte) override {
        return BlockIO::readAll(offset,buf,nbyte);
    }

    friend class Fat439Directory;

};

int strneq(const char* small, const char* big, int n) {
    for (int i=0; i<n; i++) {
        char a = small[i];
        char b = big[i];
        if (a != b) return 0;
        if (a == 0) return 1;
    }
    return big[n] == 0;
}

class Fat439Directory : public Directory {
    StrongPtr<BlockIO> dev;
    Fat439 *fs;
    uint32_t start;
    StrongPtr<File> content;
    uint32_t entries;
public:
    Fat439Directory(StrongPtr<BlockIO> dev, uint32_t start, Fat439 *fs) :
        dev(dev),
        fs(fs),
        start(start),
        content(new Fat439File(dev,start,fs))
    {
        entries = content->getLength() / 16;
    }

    uint32_t lookup(const char* name) {
        struct {
            char name[12];
            uint32_t start;
        } entry;
        for (uint32_t i=0; i<entries; i++) {
            content->readAll(16*i,&entry, 16);
            if (strneq(entry.name,name,12)) return entry.start;
        }
        return 0;
    }

    StrongPtr<File> lookupFile(const char* name) {
        uint32_t idx = lookup(name);
        if (idx == 0) return StrongPtr<File>();

        return StrongPtr<File>(new Fat439File(dev,idx,fs));
    }

    StrongPtr<Directory> lookupDirectory(const char* name) {
        uint32_t idx = lookup(name);
        if (idx == 0) return StrongPtr<Directory>();

        return StrongPtr<Directory>(new Fat439Directory(dev,idx,fs));
    }

    size_t nEntries(void) override {
        return entries;
    }

    void entryName(size_t idx, char* buf) override {
        for (int i=0; i<13; i++) {
            buf[i] = 0;
        }
        content->readAll(16*idx,buf,12);
    }

};

Fat439::Fat439(StrongPtr<BlockIO> dev) : FileSystem(dev) {
    dev->readAll(0, &super, sizeof(super));
    const uint32_t expectedMagic = 0x39333446;
    uint32_t magic;
    memcpy(&magic,super.magic,4);
    if (magic != 0x39333446) {
        Debug::panic("bad magic %x != %x",magic, expectedMagic);
    }

    fat = new uint32_t[super.nBlocks];
    dev->readAll(BlockIO::BLOCK_SIZE,fat,super.nBlocks * sizeof(uint32_t));
    rootdir = StrongPtr<Directory>(new Fat439Directory(dev,super.root,this));
}