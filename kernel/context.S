// context switching functions

// void contextSwitch(long newESP, long* saveOldESP)

	.global contextSwitch
contextSwitch:
	push %eax
	push %ecx
	mov 12(%esp),%eax     # newESP
	mov 16(%esp),%ecx     # *saveOldESP
	push %edx
	push %ebx
	push %esi
	push %edi
	push %ebp

	mov %esp,(%ecx)
	mov %eax,%esp

resume:
	pop %ebp
	pop %edi
	pop %esi
	pop %ebx
	pop %edx
	pop %ecx
	pop %eax

	ret

// void contextResult(esp)

	.global contextResume
contextResume:

	mov 4(%esp),%esp # new ESP
	jmp resume
