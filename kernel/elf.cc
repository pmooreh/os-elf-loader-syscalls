#include "elf.h"
#include "machine.h"
#include "refs.h"
#include "fs.h"
#include "vmm.h"
#include "process.h"
#include "debug.h"

int numPages(uint32_t size) {
    int ret = size / 4096;
    if (size % 4096)
        ret++;
    return ret;
}

/*
	load ELF header
	then load program headers
		if PT_LOAD, then load their data using map
*/

uint32_t ELF::load(StrongPtr<File> file) {
	// make an ELF header, note the size
    Elf32_Ehdr* elfHeader = new Elf32_Ehdr();
    size_t hdrSiz = sizeof(Elf32_Ehdr);

    // now read all on this file
    file->readAll(0, elfHeader, hdrSiz);

    // Debug::printf("    Made it into load\n");
    // Debug::printf("    entry point: %x\n", elfHeader->e_entry);
    // Debug::printf("    num pHeaders: %d\n", elfHeader->e_phnum);

    Elf32_Phdr* programHeader = new Elf32_Phdr();
    // then read each program header, mapping them into memory.
    for (uint16_t i = 0; i < elfHeader->e_phnum; i++) {
        // read in each program header
        size_t offset = elfHeader->e_phoff + i * elfHeader->e_phentsize;
        file->readAll(offset, programHeader, elfHeader->e_phentsize);

        // Debug::printf("\n");
        // Debug::printf("    prog header: %d\n", i);
        // Debug::printf("    prog header offset: %x\n", programHeader->p_offset);
        // Debug::printf("\n");

        // inspect the header. if segment type is PT_LOAD,
        // map the memory apprpriately.
        if (programHeader->p_type == PT_LOAD) {
            // StrongPtr<SegmentNode> node = StrongPtr<SegmentNode>(new SegmentNode(
            //     file, programHeader->p_vaddr, programHeader->p_offset,
            //     programHeader->p_filesz, programHeader->p_memsz));
            // Thread::current()->process->addressSpace->mapPHeader(node);

            // ATTEMPT IN A DIFFERENT MANNER
            file->readAll(programHeader->p_offset, (void*)programHeader->p_vaddr,
                programHeader->p_filesz);

            // // now map all zero's needed
            // for (uint32_t v = programHeader->p_vaddr + programHeader->p_filesz;
            //     v < programHeader->p_vaddr + programHeader->p_memsz; v++) {
            //     Thread::current()->process->addressSpace->pmap(va);
            // }
        }
    }

    // Debug::printf("DID I MAKE IT??\n");
    return elfHeader->e_entry;
}
