#ifndef _QUEUE_H_
#define _QUEUE_H_

#include "refs.h"

template <class T>
class SimpleQueue {
    T* head;
    T* tail;    
public:
    SimpleQueue() : head(nullptr), tail(nullptr) {}

    void add(T* item) {
        item->next = nullptr;
        if (tail == nullptr) {
            head = item;
            tail = item;
        } else {
            tail->next = item;
            tail = item;
        }
    }

    T* remove() {
        T* ptr = head;
        if (ptr != nullptr) {
            head = ptr->next;
            if (head == nullptr) {
                tail = nullptr;
            }
        }
        return ptr;
    }
};

template <class T>
class StrongQueue {
    StrongPtr<T> head;
    StrongPtr<T> tail;    
public:
    StrongQueue() {}

    void add(StrongPtr<T> item) {
        item->next.reset();
        if (tail.isNull()) {
            head = item;
            tail = item;
        } else {
            tail->next = item;
            tail = item;
        }
    }

    StrongPtr<T> peek() {
        return head;
    }

    StrongPtr<T> remove() {
        StrongPtr<T> ptr = head;
        if (!ptr.isNull()) {
            head = ptr->next;
            if (head.isNull()) {
                tail.reset();
            }
            ptr->next.reset();
        }
        return ptr;
    }
};

#endif
