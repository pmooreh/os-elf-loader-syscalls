#ifndef _COLLECTION_H_
#define _COLLECTION_H_

#include "refs.h"
#include "locks.h"

class BlockingLock;

template <class T>
class Collection {
	int size;
	StrongPtr<T>* arr;
	bool* allocated;
	StrongPtr<BlockingLock> lock;

	int getLowestAvailable();

public:
	Collection(int size);

	StrongPtr<T> get(int id);

	int add(StrongPtr<T> elem);

	bool remove(int id);
};

#endif
