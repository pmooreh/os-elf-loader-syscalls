#include "refs.h"
#include "device.h"
#include "fat439.h"
#include "ide.h"
#include "elf.h"

void kernelMain(void) {
    StrongPtr<BlockIO> d = IDE::d();
    StrongPtr<FileSystem> fs { new Fat439(d) };
    StrongPtr<File> init = fs->rootdir->lookupFile("init");
    uint32_t e = ELF::load(init);
    switchToUser(e,0xfffff000,0x12345678);
}

void kernelTerminate(void) {
}

