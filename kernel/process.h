#ifndef _PROCESS_H_
#define _PROCESS_H_

#include "refs.h"
#include "vmm.h"
#include "collection.h"
#include "locks.h"

class AddressSpace;
class Semaphore;
class Event;

template <class T>
class Collection;

class Process {    
public:

    /* return the kernel process */
    static StrongPtr<Process> kernel();

    /* initialize the process subsystem */
    static void init();

    /* get list of semaphores */
    static StrongPtr<Collection<Semaphore>> semaphores();

    // /* get list of events */
    // static StrongPtr<Collection<Event>> events();


    StrongPtr<AddressSpace> addressSpace;
    const int id;

    Process();
    virtual ~Process();

};

#endif
