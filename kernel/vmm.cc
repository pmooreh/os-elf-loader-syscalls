#include "vmm.h"
#include "machine.h"
#include "idt.h"
#include "libk.h"
#include "locks.h"

/**************
SegmentNode
*************/
SegmentNode::SegmentNode(StrongPtr<File> f, uint32_t v, uint32_t o,
        uint32_t sf, uint32_t sm) {
    file = f;
    va = v;
    fileOffset = o;
    segmentSizeInFile = sf;
    segmentSizeInMemory = sm;
    next = StrongPtr<SegmentNode>();
}
SegmentNode::~SegmentNode() {}

void SegmentNode::printNode() {
    Debug::printf("    va start: %x\n", va);
    Debug::printf("    offset into file: %x\n", fileOffset);
    Debug::printf("    segment size in file: %x\n", segmentSizeInFile);
    Debug::printf("    segment size in memory: %x\n", segmentSizeInMemory);
}

/**************
   SegmentList
*************/

SegmentList::SegmentList() : head(StrongPtr<SegmentNode>()),
    last(StrongPtr<SegmentNode>()),
    lock(StrongPtr<BlockingLock>(new BlockingLock())) {}
SegmentList::~SegmentList() {}

StrongPtr<SegmentNode> SegmentList::getSegmentNode(uint32_t va) {
    // assumption: list is non-empty
    lock->lock();
    StrongPtr<SegmentNode> curr = head;

    // loop through each node in LL, find one whose range includes va
    while (!curr.isNull()) {
        if (va >= curr->va && va <= (curr->va + curr->segmentSizeInMemory))
            break;
        curr = curr->next;
    }

    lock->unlock();
    return curr;
}

void SegmentList::addSegmentNode(StrongPtr<SegmentNode> node) {
    lock->lock();

    Debug::printf("\n");
    Debug::printf("    adding a node");
    node->printNode();
    Debug::printf("\n");

    if (head.isNull()) {
        head = node;
        last = node;
        lock->unlock();
        return;
    }

    // add to end
    last->next = node;
    last = node;
    lock->unlock();
    return;
}

/***********
  REST of project
**********/

struct PhysMemNode {
    PhysMemNode* next;
};

struct PhysMemInfo {
    PhysMemNode *firstFree;
    uint32_t avail;
    uint32_t limit;
    BlockingLock lock;
};

static PhysMemInfo *info = nullptr;

void PhysMem::init(uint32_t start, uint32_t end) {
    info = new PhysMemInfo;
    info->avail = start;
    info->limit = end;
    info->firstFree = nullptr;

    /* register the page fault handler */
    IDT::trap(14,(uint32_t)pageFaultHandler,3);
}

uint32_t PhysMem::alloc() {
    uint32_t p;

    info->lock.lock();

    if (info->firstFree != nullptr) {
        p = (uint32_t) info->firstFree;
        info->firstFree = info->firstFree->next;
    } else {
        if (info->avail == info->limit) {
            Debug::panic("no more frames");
        }
        p = info->avail;
        info->avail += FRAME_SIZE;
    }
    info->lock.unlock();

    bzero((void*)p,FRAME_SIZE);

    return p;
}

void PhysMem::free(uint32_t p) {
    info->lock.lock();

    PhysMemNode* n = (PhysMemNode*) p;    
    n->next = info->firstFree;
    info->firstFree = n;

    info->lock.unlock();
}

/****************/
/* AddressSpace */
/****************/

void AddressSpace::activate() {
    vmm_on((uint32_t)pd);
}

AddressSpace::AddressSpace() : lock(new BlockingLock()) {
    pd = (uint32_t*) PhysMem::alloc();
    for (uint32_t va = PhysMem::FRAME_SIZE;
        va < info->limit;
        va += PhysMem::FRAME_SIZE
    ) {
        pmap(va,va,false,true);
    }
}

AddressSpace::~AddressSpace() {
    // Debug::printf("\n");
    // Debug::printf("~ADDRESS");
    // Debug::printf("\n");
    // MISSING();

    for (int i = 512; i < 1024; i++) {
        uint32_t pdEntry = pd[i];
        if (pdEntry & 1) {
            uint32_t* pt = (uint32_t*)(pdEntry & 0xfffff000);
            //Debug::printf("    pdEntry number %d, present, entry: %x\n", i, pdEntry);

            // loop through each page table entry
            for (int j = 0; j < 1024; j++) {
                uint32_t ptEntry = pt[j];
                if (ptEntry & 1) {
                    //Debug::printf("        ptEntry number %d, present, entry: %x\n", j, ptEntry);

                    // make ptEntry a logical pointer to the frame
                    ptEntry = ptEntry & 0xfffff000;
                    // free this frame
                    PhysMem::free(ptEntry);
                }
            }
            // free this page table frame
            PhysMem::free((uint32_t)pt);
            //Debug::printf("made it out of inner loop\n");
        }
        //Debug::printf("made it out of OUTER loop!\n");
    }
    //Debug::printf("place 1\n");
    // free the pd's 0th entry? maybe later
    PhysMem::free(pd[0] & 0xfffff000);
    //Debug::printf("place 2\n");
    // free the pd itself?
    PhysMem::free((uint32_t)pd);
    //Debug::printf("place 3\n");
}

/* precondition: table is locked */
uint32_t& AddressSpace::getPTE(uint32_t va) {
    uint32_t i0 = (va >> 22) & 0x3ff;
    if ((pd[i0] & P) == 0) {
        pd[i0] = PhysMem::alloc() | 7; /* UWP */
    }
    uint32_t* pt = (uint32_t*) (pd[i0] & 0xfffff000);
    return pt[(va >> 12) & 0x3ff];
}

void AddressSpace::pmap(uint32_t va, uint32_t pa,
    bool forUser, bool forWrite) {
    
    invlpg(va);
    getPTE(va) = (pa & 0xfffff000) |
          (forUser ? U : 0) | (forWrite ? W : 0) | 1;
}

void AddressSpace::fork(StrongPtr<AddressSpace> child) {
    lock->lock();

    for (int i = 512; i < 1024; i++) {
        uint32_t pdEntry = pd[i];
        if (pdEntry & 1) {
            uint32_t* pt = (uint32_t*)(pdEntry & 0xfffff000);

            for (int j = 0; j < 1024; j++) {
                uint32_t ptEntry = pt[j];
                if (ptEntry & 1) {

                    uint32_t frame = PhysMem::alloc();
                    memcpy((void*)frame, (void*)(ptEntry & 0xfffff000), 4096);

                    uint32_t va = (i << 22) | (j << 12);
                    child->pmap(va, frame, true, true);
                }
            }
        }
    }

    lock->unlock();
    return;
}


void AddressSpace::handlePageFault(uint32_t va) {
    if (va < 0x80000000) {
        Debug::panic("bad : %x",va);
    }

    AddressSpace::lock->lock();
    // check if the mapping has been established
    uint32_t pte = getPTE(va);
    // FIX THIS!!!!
    if (pte & 1) {
        lock->unlock();
        return;
    }


    //Debug::say("page fault at %x",va);
    //Debug::printf("\n");
    // Debug::printf("call made to handlePageFault, at va: %x\n", va);
    // StrongPtr<SegmentNode> segmentNode = segmentList->getSegmentNode(va);
    // Debug::printf("    found segmentNode\n");
    // segmentNode->printNode();
    
    // get physical frame
    uint32_t frame = PhysMem::alloc();

    // load data into this frame, size is 4096 bytes
    // THIS IS PROBABLY WRONG AND NEEDS FIXING
    // NEED TO ACCOUNT FOR DIFFERENCE BETWEEN FILESZ and MEMSZ
    // size_t diff = va - segmentNode->va;
    // size_t readOffset = diff - (diff % 4096);
    // segmentNode->file->readAll(readOffset, (void*)frame, 4096);

    // now pmap that ho
    AddressSpace::pmap(va, frame, true, true);
    AddressSpace::lock->unlock();
}

extern "C" void vmm_pageFault(uintptr_t va) {
    StrongPtr<AddressSpace> as = Thread::current()->process->addressSpace;
    Thread::current()->process->addressSpace->handlePageFault(va);
}
