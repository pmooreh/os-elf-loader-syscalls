#ifndef _FAT439_H_
#define _FAT439_H_

#include "fs.h"

/**************************/
/* The FAT439 file system */
/**************************/

class Fat439 : public FileSystem {
public:
    struct {
        char magic[4];
        uint32_t nBlocks;
        uint32_t avail;
        uint32_t root;
    } super;
    uint32_t *fat;
    Fat439(StrongPtr<BlockIO> dev);
};

#endif