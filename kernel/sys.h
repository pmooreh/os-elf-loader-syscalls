#ifndef _SYSCALL_H_
#define _SYSCALL_H_

#include "refs.h"
#include "locks.h"

class SYS {
public:
    static void init(void);
    static int eventId;
    static BlockingLock* lock;
    static StrongPtr<Event>* eventArr;
};



#endif
