#ifndef _LIBC_H_
#define _LIBC_H_

#include "sys.h"

extern void putstr(char *p);
extern void putdec(unsigned long v);
extern void puthex(long v);

#endif
