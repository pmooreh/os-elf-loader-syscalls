#ifndef _SYS_H_
#define _SYS_H_

/****************/
/* System calls */
/****************/


/* print a character */
extern void putch(char c);

/* create a sempahore, return something that represents it */
extern int semaphore();

/* say up to a semaphore */
extern void up(int s);

/* say down to a semaphore */
extern void down(int s);

/* fork */
extern int fork();

/* join */
extern void join(int id);

/* shutdown */
extern void shutdown();

#endif
