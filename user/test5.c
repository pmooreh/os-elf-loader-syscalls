#include "libc.h"

#define N 1000000000

char big[N];

void show(int i) {
    putstr("*** big ");
    putdec(i);
    putstr(" = ");
    putdec(big[i]);
    putstr("\n");
}

int main() {
    putstr("*** hello\n");

    int sem = semaphore(1);

    int x = 1;
    for (int i=0; i<N; i += 100000000) {
        show(i);
        big[i] = (char) x;
        x++;
    }

    for (int i=0; i<N; i += 100000000) {
        show(i);
    }

    fork();

    down(sem);

    for (int i=0; i<N; i += 100000000) {
        big[i] ++;
        show(i);
    }


    up(sem);

    return 0;
}