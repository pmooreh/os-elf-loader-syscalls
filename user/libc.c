#include "libc.h"

static char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7',
                            '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

void putstr(char* p) {
    char c;
    while ((c = *p++) != 0) putch(c); 
}

void puthex(long v) {
    for (int i=0; i<sizeof(long)*2; i++) {
          char c = hexDigits[(v & 0xf0000000) >> 28];
          putch(c);
          v = v << 4;
    }
}

void putdec(unsigned long v) {
    if (v >= 10) {
        putdec(v / 10);
    }
    putch(hexDigits[v % 10]);
}
