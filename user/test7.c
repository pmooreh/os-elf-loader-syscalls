#include "libc.h"

int main() {
    int sem = semaphore(1);

    fork();
    fork();
    fork();
    fork();
    fork();

    down(sem);
    putstr("*** here\n");
    up(sem);

    return 0;
}