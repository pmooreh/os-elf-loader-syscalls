#include "libc.h"

#define N 2000

int main() {
    putstr("*** hello\n");
    for (int i = 0; i < N; i++) {
        int id = fork();
        if (id == 0) {
            return 0;
        }
        else {
            join(id);
            if ((i % 100) == 0) {
                putstr("*** ");
                putdec(i);
                putstr("\n");
            }
        }
    }
    putstr("*** goodbye\n");
    return 0;
}