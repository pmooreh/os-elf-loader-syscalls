#include "libc.h"

long sem = 0;
volatile long first = 1;
int limit = 4;

void sayStr(char* s) {
    down(sem);
    putstr("*** ");
    putstr(s);
    putstr("\n");
    up(sem);
}

char path[10];

void pair(int depth) {
    if (depth == limit) return;
    path[depth] = 'p';
    long id = fork();
    if (id == 0) {
        /* child */
        first = 0;
        path[depth] = 'c';
        sayStr(path);        
        pair(depth+1);
    } else {
        join(id);
        sayStr(path);
        pair(depth+1);
    }
}
        

int main() {
    putstr("*** hello\n");

    putstr("*** 3 = ");
    putdec(3);
    putstr("\n");

    putstr("*** 100 = 0x");
    puthex(100);
    putstr("\n");

    putstr("*** create semaphore\n");
    sem = semaphore(1);
    putstr("*** say down\n");
    down(sem);
    putstr("*** say up\n");
    up(sem);

    putstr("*** trying a simple fork\n");
    long id = fork();
    if (id == 0) {
        putstr("*** child\n");
        return 0;
    } else {
        join(id);
        putstr("*** parent\n");
    }

    putstr("*** let's see if semaphores work\n");
    down(sem);

    id = fork();
    if (id == 0) {
        putstr("*** child2\n");
        up(sem);
        return 0;
    } else {
        down(sem);
        putstr("*** parent2\n");
        up(sem);
        join(id);
    }
       

    putstr("*** the other way around\n");
    down(sem);

    id = fork();
    if (id == 0) {
        down(sem);
        putstr("*** child3\n");
        up(sem);
        return 0;
    } else {
        putstr("*** parent3\n");
        up(sem);
        join(id);
    }


    putstr("*** going for the big one\n");
    pair(0);

    if (first) {
        shutdown();
    }
    return 0;
}
