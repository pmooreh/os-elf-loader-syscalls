default : all;

OK = $(sort $(wildcard test?.ok))
TESTS = $(patsubst %.ok,%,$(OK))

test : $(TESTS);

$(TESTS) : test% : all
	@make -C fat439 clean
	@cp user/test$* fat439/init
	@rm -f fat439/user.img
	@make -C fat439 user.img
	-@echo -n "$@ ... "
	@expect e0.tcl > $@.raw
	-@egrep '^\*\*\*' $@.raw > $@.out 2>&1
	-@((diff -b $@.out $@.ok > $@.diff 2>&1) && echo "pass") || echo "failed, look at $@.raw, $@.ok, $@.out, and $@.diff for more information"
	
% :
	(make -C kernel $@)
	(make -C user $@)
	(make -C fat439 $@)
